﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trapRelease : MonoBehaviour
{
    
    public void activate()
    {
        StartCoroutine(MoveToPosition(new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z), 1.0f));
    }

    private IEnumerator MoveToPosition(Vector3 newPosition,  float time)
    {
        float  elapsedTime = 0;
        Vector3  startingPos = transform.position;
        while (elapsedTime < time)
        {
            transform.position = Vector3.Lerp(startingPos, newPosition, (elapsedTime / time));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();

        }
    }

}
