﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class sliderGravity : MonoBehaviour
{

    public Slider mainSlider;
    public Text sliderText;

    public void SubmitSliderSetting()
    {
        //Displays the value of the slider in the console.

        Physics.gravity = new Vector3(0, -mainSlider.value, 0);
        sliderText.text = "Gravity : " + mainSlider.value.ToString(".00");
        Debug.Log(mainSlider.value);
    }
}
