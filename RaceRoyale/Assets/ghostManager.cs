﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public struct GhostTransform
{
    public Vector3 position;
    public Quaternion rotation;

    public GhostTransform(Transform transform)
    {
        this.position = transform.position;
        this.rotation = transform.rotation;
    }
}
public class ghostManager : MonoBehaviour
{

    public Transform player;
    public Transform ghostPlayer;

    public bool recording;
    public bool playing;
    public bool save;
    public bool load;
    public bool ownGhost;
    public const string GHOST_DATA_KEY = "GHOST_DATA_KEY";
   [SerializeField]
    WebAccess wa;
    private List<GhostTransform> recordedGhostTransforms = new List<GhostTransform>();
    private List<GhostTransform> loadedGhostTransforms = new List<GhostTransform>();

    private GhostTransform lastRecordedGhostTransform;

    // Start is called before the first frame update
    void Start()
    {


        //StreamWriter writer = new StreamWriter("BilalGhost2.txt", true);
        //writer.Write(PlayerPrefs.GetString(GHOST_DATA_KEY));
        if (ownGhost) {
            this.Load(GHOST_DATA_KEY);
            this.Play();

        }
        else
        {
            
        }


    }

    // Update is called once per frame
 
    void FixedUpdate()
    {

        if(recording == true)
        {
           
                GhostTransform newGhostTransform = new GhostTransform(player);
                recordedGhostTransforms.Add(newGhostTransform);

                lastRecordedGhostTransform = newGhostTransform;
         }

        if(playing == true)
        {
            Play();
        }
        if(save == true)
        {
            Save();
            save = false;
        }
        if(load == true)
        {
            Load(GHOST_DATA_KEY);
            load = false;
        }
    }

    void Play()
    {

        ghostPlayer.gameObject.SetActive(true);
        StartCoroutine(StartGhost());
        playing = false;
    }

    IEnumerator StartGhost()
    {
        for(int i =0;i < loadedGhostTransforms.Count;i++)
        {
            ghostPlayer.position = loadedGhostTransforms[i].position;
            ghostPlayer.rotation = loadedGhostTransforms[i].rotation;

            yield return new WaitForFixedUpdate();
        }
    }

    public void Save()
    {

        string saveString = PlayerPrefs.GetFloat(winScript.PLAYER_HIGHSCORE_KEY).ToString() + "t";
        for (int i = 0; i < recordedGhostTransforms.Count; i++)
        {
            saveString += recordedGhostTransforms[i].position.ToString("0.000");
            saveString += "a";
            saveString += recordedGhostTransforms[i].rotation.ToString("0.000");
            saveString += "b";

        }

        PlayerPrefs.SetString(GHOST_DATA_KEY, saveString);
        PlayerPrefs.Save();
    }

    void Load(string key)
    {
        if(!PlayerPrefs.HasKey(key))
        {
            return;
        }
        string loadString = PlayerPrefs.GetString(key);
        string[] loadString2 = loadString.Split('t');

        string[] transformArray;
        transformArray = loadString2[1].Split('b');

        GhostTransform loadGhostTranform;

        for(int i = 0; i < transformArray.Length;i++ )
        {
            string[] splitArray = null;
            splitArray = transformArray[i].Split('a');
            if(splitArray.Length == 2) { 
               

                var positionArray = splitArray[0].Split(',');

                positionArray[0] = positionArray[0].Replace("(", "");
                positionArray[2] = positionArray[2].Replace(")", "");
               
                loadGhostTranform.position = new Vector3(float.Parse(positionArray[0]), float.Parse(positionArray[1]), float.Parse(positionArray[2]));

                string[] rotationArray = splitArray[1].Split(',');

                rotationArray[0] = rotationArray[0].Replace("(", "");
                rotationArray[3] = rotationArray[3].Replace(")", "");
                loadGhostTranform.rotation = new Quaternion(float.Parse(rotationArray[0]), float.Parse(rotationArray[1]), float.Parse(rotationArray[2]), float.Parse(rotationArray[3]));

                loadedGhostTransforms.Add(loadGhostTranform);
            }

            

        }



    }

   public void readGhostFromFile(string data)
   {

       
        string loadString = data;
        string[] loadString2 = loadString.Split('t');
        if(loadString2[0] == PlayerPrefs.GetFloat(winScript.PLAYER_HIGHSCORE_KEY).ToString())
        {
            return;
        }
        string[] transformArray;
        transformArray = loadString2[1].Split('b');

        GhostTransform loadGhostTranform;

        for (int i = 0; i < transformArray.Length; i++)
        {
            string[] splitArray = null;
            splitArray = transformArray[i].Split('a');
            if (splitArray.Length == 2)
            {
             

                var positionArray = splitArray[0].Split(',');

                positionArray[0] = positionArray[0].Replace("(", "");
                positionArray[2] = positionArray[2].Replace(")", "");
    
                loadGhostTranform.position = new Vector3(float.Parse(positionArray[0]), float.Parse(positionArray[1]), float.Parse(positionArray[2]));

                string[] rotationArray = splitArray[1].Split(',');

                rotationArray[0] = rotationArray[0].Replace("(", "");
                rotationArray[3] = rotationArray[3].Replace(")", "");
                loadGhostTranform.rotation = new Quaternion(float.Parse(rotationArray[0]), float.Parse(rotationArray[1]), float.Parse(rotationArray[2]), float.Parse(rotationArray[3]));

                loadedGhostTransforms.Add(loadGhostTranform);
            }



        }
        this.Play();
    }
}
