﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class frictionSlider : MonoBehaviour
{

    public Collider playerCollider;
    // Start is called before the first frame update
    public Slider mainSlider;
    public Text sliderText;

    public void SubmitSliderSetting()
    {
        //Displays the value of the slider in the console.
        playerCollider.material.dynamicFriction = mainSlider.value;
        playerCollider.material.staticFriction = mainSlider.value;
        sliderText.text = "Friction : " + mainSlider.value.ToString(".00");
        Debug.Log(mainSlider.value);
    }
}
