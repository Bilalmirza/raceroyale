﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseColor : MonoBehaviour
{

    [SerializeField] Camera mainCamera;
    [SerializeField] GameObject car;
    [SerializeField] winScript wScript;
    [SerializeField] ghostManager _ghostManager;
    // Start is called before the first frame update
    public void Start()
    {
    }
    public void Trigger()
    {
        wScript = FindObjectOfType<winScript>();
        MultipleTargetCamera mtc = mainCamera.GetComponent<MultipleTargetCamera>();
        mtc.targets.Clear();
        mtc.targets.Add(car.transform);
        mtc.targets.Add(car.GetComponentInChildren<DirectionForCamera>().transform);
        car.AddComponent<playerScript>();
        playerScript ps = car.GetComponent<playerScript>();
        ps.car = car.GetComponentInChildren<carScript>().gameObject;
        GameObject.Find("Slider_Character_Exp_LevelUp").GetComponent<PlayerVecolity>().playerBody = car.GetComponent<Rigidbody>();
        GameObject.Find("Slider_Character_Exp").GetComponent<BoostValueDisplay>().pScript = ps;
        car.layer = 10;
        car.GetComponent<AIScript>().enabled = false;
        _ghostManager.player = ps.car.transform;
        _ghostManager.recording = true;
        if(wScript)
        {
            wScript.playerColor = ps.car.GetComponent<carScript>().CarColor;
        }
    }

}
