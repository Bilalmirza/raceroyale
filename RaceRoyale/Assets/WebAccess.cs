﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Net;
using System.IO;

public class WebAccess : MonoBehaviour
{
    // Start is called before the first frame update



    string FTPUserName = "admin_werplay";
     string FTPPassword = "werplay123";
     string FilePath;
     string FTPHost = "ftp://159.89.227.2/public_html/raceroyale/";
    public bool downloadComplete;
    [SerializeField] ghostManager gh;
    public void UploadFile()
    {
        Debug.Log("Path: " + FilePath);


        WebClient client = new System.Net.WebClient();
        Uri uri = new Uri(FTPHost + new FileInfo(FilePath).Name);
        Debug.Log(uri.ToString());
        client.UploadProgressChanged += new UploadProgressChangedEventHandler(OnFileUploadProgressChanged);
        client.UploadFileCompleted += new UploadFileCompletedEventHandler(OnFileUploadCompleted);
        client.Credentials = new System.Net.NetworkCredential(FTPUserName, FTPPassword);
        client.UploadFileAsync(uri, "STOR", FilePath);
    }

    void OnFileUploadProgressChanged(object sender, UploadProgressChangedEventArgs e)
    {
        Debug.Log("Uploading Progreess: " + e.ProgressPercentage);
    }

    void OnFileUploadCompleted(object sender, UploadFileCompletedEventArgs e)
    {

        if(e.Error != null)
        {
            Debug.Log("Uploading Progreess: " + e.Error);
        }
        else {
            Debug.Log("File Uploaded");

        }

    }

    public void CreateText()
    {
        File.WriteAllText(FilePath, PlayerPrefs.GetString(ghostManager.GHOST_DATA_KEY));
        this.UploadFile();
        
    }

    void Start()
    {
        FilePath = Application.persistentDataPath + "/ghostData.txt";
        //CreateText();
        StartCoroutine(getGhostDataOnline());
    }

    public string url = "http://werplay.com/raceroyale/data2.txt";
    public string downloadedString = "";
    public float ghostScore;
    IEnumerator getGhostDataOnline()
    {
        using (WWW www = new WWW("http://werplay.com/raceroyale/ghostData.txt"))
        {
            yield return www;
            downloadedString = www.text.ToString();
            ghostScore = float.Parse(downloadedString.Split('t')[0]);
            downloadComplete = true;
            //Debug.Log("GHOST DATA DOWNLOAD: " + www.text.ToString());
            //Debug.Log("Ghost score " + ghostScore.ToString("#.00"));
            gh.readGhostFromFile(downloadedString);
        }
    }

    public string getDownloadedString()
    {
        return downloadedString;
    }

    public string getGhostDataTime()
    {
        if (getDownloadedString() != "")
        {

            string[] splitString = getDownloadedString().Split('t'); ;
            return splitString[0];
        }
        return "";
        
    }
}

    // Update is called once per frame
   