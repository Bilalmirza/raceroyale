﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(CollisionOverlap))]
public class ChangeCarState : TriggerOperation
{
    // Start is called before the first frame update

    [SerializeField] AIScript.carStates changeStateTo;
    public override void TriggerInvoker(GameObject obj)
    {

        obj.GetComponent<AIScript>().currentState = changeStateTo;

    }
};
