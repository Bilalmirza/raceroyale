﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIScript : MonoBehaviour
{

     Rigidbody rigidBody;

    
    public enum carStates
    {
        boosting,
        breaking
    }

    public carStates currentState;
    // Start is called before the first frame update
    void Start()
    {
        rigidBody = this.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if(currentState == carStates.boosting)
        {
            rigidBody.drag = 0.0f;
        }
        else if(currentState == carStates.breaking)
        {
            rigidBody.drag = 1.0f;
        }
    }
}
