﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayHighScore : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Text>().text = "Highscore:" + PlayerPrefs.GetFloat("PLAYER_HIGHSCORE_KEY").ToString("#.00");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
