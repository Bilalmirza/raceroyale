﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnStairs : MonoBehaviour
{

    [SerializeField] GameObject stair;
    [SerializeField] GameObject start;
    [SerializeField] GameObject end;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(CreateStairs());
    }

    public IEnumerator CreateStairs()
    {

        yield return new WaitForSeconds(3.0f);
        GameObject spawnedObject = Instantiate(stair);

        MoveTo moveTo = spawnedObject.GetComponent<MoveTo>();
        moveTo.startPosition = start;
        moveTo.endPosition = end;
        moveTo.startAction();
        moveTo.EndEvent.AddListener(spawnedObject.GetComponent<DestroySelf>().Trigger);

        yield return StartCoroutine(CreateStairs());

    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
