﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class stickToPlatform : MonoBehaviour
{
    // Start is called before the first frame update
    private Quaternion startRotation;

    List<GameObject> _listOfCars;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //for (int i = 0; i < _listOfCars.Count; i++)
        //{
        //    if (Vector3.Distance(_listOfCars[i].transform.position, this.transform.position) > this.transform.localScale.x /2 )
        //    {
        //        _listOfCars[i].GetComponent<playerScript>().car.GetComponent<carScript>().isStuck = false;
        //        _listOfCars.RemoveAt(i);
        //        return;

        //    }
        //}
    }


    void OnCollisionEnter(Collision collisionInfo)
    {

        GameObject ball = collisionInfo.collider.gameObject;
        startRotation = this.transform.rotation;
        GameObject car = ball.GetComponent<playerScript>().car.gameObject;
       
        if (car.GetComponent<carScript>().isStuck)
        {
            return;
        }



        ball.transform.SetParent(this.transform.parent.transform, true);
        ball.GetComponent<Rigidbody>().useGravity = false;
        ball.GetComponent<Rigidbody>().velocity = Vector3.zero;
        ball.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        car.GetComponent<carScript>().isStuck = true;

        car.transform.localRotation = Quaternion.identity;
        ball.transform.LookAt(this.transform, Vector3.up);
        ball.transform.localRotation *= Quaternion.Euler(0, this.transform.parent.GetComponent<PlatformRotate>().speed > 0 ? 90 : -90, 0);
    }

    void OnCollisionExit(Collision collisionInfo)
    {

        //GameObject ball = collisionInfo.collider.gameObject;
        //GameObject car = ball.GetComponent<playerScript>().car.gameObject;

        //if (!car.GetComponent<carScript>().isStuck)
        //{
        //    return;
        //}
        //else
        //{
        //    car.GetComponent<carScript>().isStuck = false;
        // }
    }
}
