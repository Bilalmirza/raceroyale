﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawner : MonoBehaviour
{

    bool isTouching = true;
    float fallCounter = 0;
    [SerializeField] float TimeToRespawn = 2.5f;
    Vector3[] lastTouchPosition = new Vector3[20];
    int doAddLastPosition;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void FixedUpdate()
    {
        isTouching = false;

    }

    private void OnCollisionStay(Collision collision)
    {
        isTouching = true;
        doAddLastPosition++;
        if (doAddLastPosition % 3 == 0)
        {
            for (int i = 0; i < lastTouchPosition.Length - 1; i++)
            {
                lastTouchPosition[i] = lastTouchPosition[i + 1];
            }
            lastTouchPosition[lastTouchPosition.Length - 1] = this.transform.position;
        }
    }

    private void Update()
    {
        if(!isTouching)
        {
            fallCounter += Time.deltaTime;
            if(fallCounter > TimeToRespawn)
            {
                Respawn();
            }
            
        }
        else
        {
            fallCounter = 0;
        }


    }

    void Respawn()
    {
        Rigidbody rigidbody = this.GetComponent<Rigidbody>();
        rigidbody.velocity = Vector3.zero;
        rigidbody.angularVelocity = Vector3.zero;
        rigidbody.isKinematic = true;
        rigidbody.detectCollisions = false;
        StartCoroutine(RespawnEnd());
        

        fallCounter = 0;
        this.transform.position = lastTouchPosition[0];

       
    }

    IEnumerator RespawnEnd()
    {
        yield return new WaitForSeconds(1.25f);
        Rigidbody rigidbody = this.GetComponent<Rigidbody>();
        rigidbody.isKinematic = false;
        rigidbody.detectCollisions = true;

    }
}
