﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerVecolity : MonoBehaviour
{
    // Start is called before the first frame update
    public Rigidbody playerBody;
    [SerializeField] public Slider _slider;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (playerBody)
        {
            _slider.value = playerBody.velocity.magnitude / 6.0f;
        }
    }
}
