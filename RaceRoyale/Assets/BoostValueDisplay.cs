﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoostValueDisplay : MonoBehaviour
{
    // Start is called before the first frame update

    public playerScript pScript;
    [SerializeField] Slider _slider;
    [SerializeField] Image FillImage;
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (pScript)
        {
            _slider.value = 1.0f - pScript.boostCD / 2.0f;
            if(_slider.value > 0.98)
            {
                FillImage.color = Color.white;
            }
            else
            {
                FillImage.color = Color.gray;
            }
        }
    }
}
