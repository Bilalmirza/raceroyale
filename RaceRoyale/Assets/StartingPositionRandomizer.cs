﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartingPositionRandomizer : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Vector3 position = this.gameObject.transform.position;
        this.transform.position = new Vector3(position.x + Random.Range(-0.05f, 0.05f), position.y , position.z);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
