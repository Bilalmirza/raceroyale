﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveLift : MonoBehaviour
{
    private bool isRevert = true;
    Quaternion end_Rotation = Quaternion.Euler(110,0f, 0f);
    Quaternion start_Rotation = Quaternion.Euler(360,0f,0f);
    Quaternion end_Rot = new Quaternion();
    Quaternion start_Rot = new Quaternion();

    public IEnumerator Move(Transform start_Position, Transform end_Position, float completion_Time)
    {
        float timeElapsed = 0f;
        float rotation_Elapsed_Time = 0f;
        Vector3 start_Pos = start_Position.position;

        while (true)
        {
            Vector3 end_Pos = isRevert ? end_Position.position : start_Position.position;
            while (timeElapsed < completion_Time)
            {
                gameObject.transform.position = Vector3.Lerp(start_Pos, end_Pos, timeElapsed / completion_Time);
                timeElapsed += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }

            start_Rot = isRevert ? start_Rotation : end_Rotation;
            end_Rot = isRevert ? end_Rotation : start_Rotation;
            while (rotation_Elapsed_Time <= 0.3f)
            {
                gameObject.transform.rotation = Quaternion.Lerp(start_Rot, end_Rot, rotation_Elapsed_Time / 0.3f);
                rotation_Elapsed_Time += Time.deltaTime;
                yield return null;
            }
            gameObject.transform.rotation = end_Rotation;
            rotation_Elapsed_Time = 0f;
            timeElapsed = 0f;
            start_Pos = end_Pos;
            isRevert = !isRevert;
        }

    }
}
