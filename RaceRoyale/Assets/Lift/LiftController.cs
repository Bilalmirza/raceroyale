﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiftController : MonoBehaviour
{

    public GameObject liftPrefab;
    public int lift_Amount = 1;
    private List<GameObject> lifts = new List<GameObject>();
    public Transform start_Position;
    public Transform end_Position;
    public float completion_Time = 2f;
   
    void Start()
    {
        SpawnPaths();
        StartCoroutine(lifts[0].GetComponent<MoveLift>().Move(start_Position, end_Position, completion_Time ) );
        StartCoroutine( GeneratePath() );
    }

    private void SpawnPaths()
    {
        for (int i = 0; i < lift_Amount; i++)
        {
            GameObject path = Instantiate(liftPrefab, Vector3.one, Quaternion.identity);
            lifts.Add(path);
        }
    }

    private IEnumerator GeneratePath()
    {
        float timeElapsed = 0f;
        int currentLiftIndex = 1;
        float totalTime = completion_Time / lift_Amount;
        while (currentLiftIndex < lifts.Count)
        {
            while (timeElapsed < totalTime)
            {
                timeElapsed += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            timeElapsed = 0f;
            StartCoroutine(lifts[currentLiftIndex].GetComponent<MoveLift>().Move(start_Position, end_Position, completion_Time));
            currentLiftIndex++;
        }
       
    }
 
}
