﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class limitSpeed : MonoBehaviour
{

    Rigidbody _rigidBody;
    float maximumVelocity;
    // Start is called before the first frame update
    void Start()
    {
        _rigidBody = this.GetComponent<Rigidbody>();
        maximumVelocity = 6.0f;


    }

    // Update is called once per frame
    void Update()
    {
        LimitVelocity();
    }
    private void LimitVelocity()
    {
        //if (this._rigidBody.velocity.magnitude > maximumVelocity)
        //{
        //    Vector3 normalizedValue = this._rigidBody.velocity.normalized;
        //    _rigidBody.velocity = normalizedValue * maximumVelocity;
        //}

        if (this._rigidBody.velocity.magnitude > maximumVelocity)
        {
            this._rigidBody.velocity = Vector3.ClampMagnitude(this._rigidBody.velocity, maximumVelocity);
        }
    }
}
