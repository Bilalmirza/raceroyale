﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MergeVertices
{

    //public GameObject inputGameobject;

	// Use this for initialization
	//void Start ()
   // {
     //   inputGameobject.GetComponent<MeshFilter>().mesh = MergeVerticesOnThisMesh(inputGameobject.GetComponent<MeshFilter>().mesh);
	//}
	
	// Update is called once per frame
	//void Update () {
	//	
	//}

    public static void MergeVerticesOnThisMesh(Mesh mesh)
    {
        int i = 0;
        List<Vector3> vertices = new List<Vector3>();
        List<Vector2> uvs = new List<Vector2>();
        List<Vector3> normals = new List<Vector3>();
        List<int> vertexIndicesToRemove = new List<int>();

        int[] triangles = mesh.triangles;
        //CustomDebug(triangles);

        for (i = 0; i < mesh.vertices.Length; i++)
        {
            vertices.Add(mesh.vertices[i]);
            uvs.Add(mesh.uv[i]);
            normals.Add(mesh.normals[i]);
        }

        int numAlreadyRemoved = 0;
        int currentVertexIndex = 0;

        int currentIndexToRemove = 0;

        while(currentVertexIndex<vertices.Count)
        {
            vertexIndicesToRemove.Clear();
            for (i = currentVertexIndex+1; i < vertices.Count; i++)
            {
                if(vertices[currentVertexIndex]==vertices[i])
                    vertexIndicesToRemove.Add(i);
            }

            numAlreadyRemoved = 0;
            for (i = 0; i < vertexIndicesToRemove.Count; i++)
            {
                currentIndexToRemove = vertexIndicesToRemove[i]-numAlreadyRemoved;
                vertices.RemoveAt(currentIndexToRemove);
                uvs.RemoveAt(currentIndexToRemove);
                normals.RemoveAt(currentIndexToRemove);

                for (int j = 0; j < triangles.Length; j++)
                {
                    if(triangles[j]==currentIndexToRemove)
                        triangles[j] = currentVertexIndex;
                    else if(triangles[j]>currentIndexToRemove)
                        triangles[j] = triangles[j] - 1;
                }

                numAlreadyRemoved++;
            }

            /*numAlreadyRemoved = 0;
            for (i = 0; i < vertexIndicesToRemove.Count; i++)
            {
                for (int j = 0; j < triangles.Length; j++)
                {
                    if(triangles[j]>vertexIndicesToRemove[i]-numAlreadyRemoved)
                        triangles[j] = triangles[j] - 1;
                }

                numAlreadyRemoved++;
            }*/
            currentVertexIndex++;
        }

        //Mesh finalMesh = new Mesh();
        mesh.Clear();
        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles;
        mesh.normals = normals.ToArray();
        mesh.uv = uvs.ToArray();

    }
}
