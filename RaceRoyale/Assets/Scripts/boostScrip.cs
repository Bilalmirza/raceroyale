﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boostScrip : MonoBehaviour
{

    //public GameObject parent;

    private float _time;
    // Start is called before the first frame update
    void Start()
    {
        _time = 0.0f;

    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.parent)
        {
            //parent = this.transform.parent.gameObject;
            this.transform.position = this.transform.parent.transform.position;
            this.transform.rotation = this.transform.parent.transform.rotation;
        }
        _time += Time.deltaTime;
        if(_time > 1.4f/2.35f)
        {
            Destroy(gameObject);

        }
    }
}
