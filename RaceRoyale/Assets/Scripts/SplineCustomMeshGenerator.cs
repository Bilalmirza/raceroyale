﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SplineHelperFunctions;

public class SplineCustomMeshGenerator : MonoBehaviour
{
    public Transform customMeshTransform;
    public Transform splineParent;
    private Transform finalTransform;
    //public Material pathMaterial;
    //public float tesselationFactor = 1.0f;
    private Vector3[] splinePoints;
    private float pathLength = 10.0f;
    [SerializeField] GameObject parent;

	// Use this for initialization
	void Start ()
    {
        finalTransform = new GameObject("TestMesh").transform;
        finalTransform.gameObject.AddComponent<MeshFilter>();
        finalTransform.gameObject.AddComponent<MeshRenderer>();
        DoEverything();
        finalTransform.gameObject.AddComponent<MeshCollider>();
        finalTransform.SetParent(parent.transform);

    }

    private void DoEverything()
    {
        splinePoints = SplineHelper.GetSplinePoints(splineParent);
        pathLength = SplineHelper.GetPathLength(splinePoints);
        splinePoints = SplineHelper.ParameterizeCPs(splinePoints);
        DuplicateAndCombineCustomMesh();
        LayoutCustomMeshOnSpline();
    }
	
	// Update is called once per frame
	void Update ()
    {
        //DoEverything();
	}

    private void LayoutCustomMeshOnSpline()
    {
        Vector3 centerPoint = Vector3.zero;
        Vector3 forwardVector = Vector3.zero;
        Vector3 crossProductVector = Vector3.zero;
        float t = 0;
        Vector3 tempVector = Vector3.zero;

        finalTransform.position = splinePoints[0];

        Vector3[] vertices = finalTransform.GetComponent<MeshFilter>().mesh.vertices;
        for (int i = 0; i < vertices.Length; i++)
        {
            t = vertices[i].z / pathLength;
            centerPoint = SplineHelper.Interp(splinePoints, t) - splinePoints[0];
            forwardVector = SplineHelper.GetRotationVectorAtPoint(splinePoints, t);
            crossProductVector = Vector3.Cross(forwardVector, Vector3.up);
            tempVector = centerPoint + crossProductVector.normalized * -vertices[i].x;
            vertices[i] = new Vector3(tempVector.x, centerPoint.y + vertices[i].y, tempVector.z);
        }

        finalTransform.GetComponent<MeshFilter>().mesh.vertices = vertices;
        finalTransform.GetComponent<MeshFilter>().mesh.RecalculateBounds();
    }

    private void DuplicateAndCombineCustomMesh()
    {
        float meshLength = GetMeshLength(customMeshTransform);
        int numDuplicates = Mathf.FloorToInt(pathLength / meshLength);
        MeshFilter[] meshFilters = new MeshFilter[numDuplicates];
        //meshFilters[0] = customMeshTransform.GetComponent<MeshFilter>();
        Transform tempTransform = null;

        for (int i = 0; i < numDuplicates; i++)
        {
            tempTransform = Instantiate(customMeshTransform);
            tempTransform.position = customMeshTransform.position + new Vector3(0, 0, meshLength * i);
            meshFilters[i] = tempTransform.GetComponent<MeshFilter>();
        }

        CombineInstance[] combine = new CombineInstance[meshFilters.Length];

        for (int i = 0; i < meshFilters.Length; i++)
        {
            combine[i].mesh = meshFilters[i].sharedMesh;
            combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
        }

        finalTransform.GetComponent<MeshFilter>().mesh = new Mesh();
        finalTransform.GetComponent<MeshFilter>().mesh.CombineMeshes(combine);
        MergeVertices.MergeVerticesOnThisMesh(finalTransform.GetComponent<MeshFilter>().mesh);
        finalTransform.GetComponent<Renderer>().sharedMaterial = customMeshTransform.GetComponent<Renderer>().sharedMaterial;

        for (int i = 0; i < meshFilters.Length; i++)
        {
            Destroy(meshFilters[i].gameObject);
        }
    }

    private float GetMeshLength(Transform inputTransform)
    {
        return inputTransform.GetComponent<MeshRenderer>().bounds.size.z;
    }
}
