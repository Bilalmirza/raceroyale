﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class platformRotateRandom : MonoBehaviour
{
    // Start is called before the first frame update
    float speed = 1.0f;
    void Start()
    {

        if(Random.Range(0,1) < 0.5f)
        {
            speed = Random.Range(-5,-2);

        }
        else
        {
            speed = Random.Range(2, 5);

        }
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Rotate(new Vector3(0, speed * Time.deltaTime * 60, 0));

    }
}
