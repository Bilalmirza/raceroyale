﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class CollisionOverlap : MonoBehaviour
{

    [SerializeField] public LayerMask _layerMask = new LayerMask();
    [SerializeField] public UnityEvent _trigger = new UnityEvent();

    private void OnCollisionEnter(Collision collision)
    {
        if(((1 << collision.gameObject.layer) & _layerMask)  != 0)
        {
            _trigger?.Invoke();
            TriggerOperation triggerOperation = this.GetComponent<TriggerOperation>();
            if (triggerOperation)
            {
                triggerOperation.TriggerInvoker(collision.gameObject);
            }
        }
    }



}



