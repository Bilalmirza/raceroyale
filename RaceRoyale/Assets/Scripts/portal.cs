﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class portal : MonoBehaviour
{

    [SerializeField]
    GameObject end;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnCollisionEnter(Collision collisionInfo)
    {

        //if (collisionInfo.collider.tag == "green")
        {
            Vector3 velocity = collisionInfo.collider.GetComponent<Rigidbody>().velocity;

            collisionInfo.collider.transform.position = end.transform.position;
            collisionInfo.collider.GetComponent<Rigidbody>().velocity = velocity;
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        //if (collider.tag == "green")
        {
            Vector3 velocity = collider.GetComponent<Rigidbody>().velocity;

            collider.transform.position = end.transform.position;
            collider.GetComponent<Rigidbody>().velocity = velocity;
        }
    }
}
