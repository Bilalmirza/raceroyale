﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SplineHelperFunctions;

public class SplineMeshGenerator : MonoBehaviour
{
    public Transform splineParent;
    public Material pathMaterial;
    public float tesselationFactor = 1.0f;
    private Vector3[] splinePoints;
    private float pathLength = 10.0f;

	// Use this for initialization
	void Start ()
    {
        DoEverything();
	}
	
    void DoEverything()
    {
        splinePoints = SplineHelper.GetSplinePoints(splineParent);
        pathLength = SplineHelper.GetPathLength(splinePoints);
        splinePoints = SplineHelper.ParameterizeCPs(splinePoints);
        GameObject.Find("TestMesh").transform.position = SplineHelper.Interp(splinePoints,0);
        GenerateMesh();
    }

	// Update is called once per frame
	void Update ()
    {
        //DoEverything();
        //splinePoints = SplineHelper.GetSplinePoints(splineParent);

        //GenerateMesh();
        //if(Input.GetKey(KeyCode.UpArrow))
          //  t+=0.001f;
	}

    void GenerateMesh()
    {
        Mesh myMesh = new Mesh();
        Vector3[] vertices;
        int[] triangles;
        Vector2[] uvs;

        int numSegments = (int)(pathLength * tesselationFactor);
        float pathWidth = 1.0f;
        float t = 0;
        float incrementT = 1.0f / (float)numSegments;
        Vector3 centerPoint = Vector3.zero;
        Vector3 forwardVector = Vector3.zero;
        Vector3 crossProductVector = Vector3.zero;

        vertices = new Vector3[(numSegments+1)*2];


        for (int i = 0; i < numSegments+1; i++)
        {
            centerPoint = SplineHelper.Interp(splinePoints, t) - splineParent.position;
            forwardVector = SplineHelper.GetRotationVectorAtPoint(splinePoints, t);
            crossProductVector = Vector3.Cross(forwardVector, Vector3.up);
            vertices[i*2 + 0] = centerPoint + crossProductVector.normalized * pathWidth;
            vertices[i*2 + 1] = centerPoint - crossProductVector.normalized * pathWidth;
            t+=incrementT;
            //vertices[i*4 + 2] = new Vector3(0, 0, 1);
            //vertices[i*4 + 3] = new Vector3(1, 0, 1);
        }

        uvs = new Vector2[vertices.Length];
        //Debug.Log(pathLength);
        for (int i = 0; i < uvs.Length; i+=2)
        {
            uvs[i] = new Vector2(0, i*0.15f/tesselationFactor);
            uvs[i+1] = new Vector2(1, i*0.15f/tesselationFactor);
        }

        triangles = new int[numSegments*2*3];

        for (int i = 0; i < numSegments; i++)
        {
            triangles[i*6 + 0] = 1 + 2*i;
            triangles[i*6 + 1] = 0 + 2*i;
            triangles[i*6 + 2] = 2 + 2*i;
            triangles[i*6 + 3] = 1 + 2*i;
            triangles[i*6 + 4] = 2 + 2*i;
            triangles[i*6 + 5] = 3 + 2*i;
        }

        myMesh.vertices = vertices;
        myMesh.uv = uvs;
        myMesh.triangles = triangles;
        //MeshTopology myTopolgy = myMesh.GetTopology(0);
        //myTopolgy = MeshTopology.Lines;
        //myMesh.SetIndices(myMesh.GetIndices(0), myTopolgy, 0);

        GameObject.Find("TestMesh").GetComponent<MeshFilter>().mesh = myMesh;
        GameObject.Find("TestMesh").GetComponent<Renderer>().sharedMaterial = pathMaterial;
    }
}
