﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TimerTrigger : MonoBehaviour
{
    [SerializeField] public float duration = 1.0f;
    [SerializeField] private UnityEvent onTimerEnd = new UnityEvent();

    private void Start()
    {
        StartCoroutine(StartTimer());
    }

    private IEnumerator StartTimer()
    {
        yield return new WaitForSeconds(duration);
        onTimerEnd?.Invoke();
    }
}
