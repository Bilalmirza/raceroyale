﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class boostCar : TriggerOperation
{
    // Start is called before the first frame update

    //[SerializeField] public UnityEvent _trigger = new UnityEvent();
    [SerializeField] float boostSpeed = 2.0f;

    public override void TriggerInvoker(GameObject obj)
    {

        BoostUp(obj.GetComponent<Rigidbody>(), boostSpeed, -this.gameObject.transform.forward);

    }
    public void BoostUp(Rigidbody rigidbody,float boostValue, Vector3 direction)
    {
        rigidbody.velocity = direction;
        rigidbody.velocity = rigidbody.velocity.normalized * boostValue;
    }
}
