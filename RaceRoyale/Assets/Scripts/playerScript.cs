﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Respawner))]

public class playerScript : MonoBehaviour
{
    [SerializeField]
    public GameObject boostObj;
    [SerializeField]
    public GameObject car;

    [SerializeField]
    GameObject CameraDistanceOffset;

    Rigidbody _rigidBody;
    float maximumVelocity;
    float _boostAddition;
    float sqrMaxVelocity;
    public float boostCD = 2.0f;

    float tapCd = 0.5f;
    int tapCount = 0;
    TrailRenderer[] trailRenderers;
    // Start is called before the first frame update
    void Start()
    {
        boostObj = GameObject.Find("Boost");
        _rigidBody = this.GetComponent<Rigidbody>();
        maximumVelocity = 6.5f;
        _boostAddition = 0.0f;
        sqrMaxVelocity = maximumVelocity * maximumVelocity;
        trailRenderers =  this.gameObject.GetComponentsInChildren<TrailRenderer>();
        GameManager.getInstance().logThis();

    }

    // Update is called once per frame
    void Update()
    {
        boostCD -= Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (tapCount == 1 && tapCd > 0.0f)
            {
                //double tap
                boost();
            }
            else
            {
                //single tap
                tapCd = 0.5f;
                tapCount++;
                Brakes(true);
            }
        }

        if(Input.GetKeyUp(KeyCode.Space))
        {
            Brakes(false);
        }

        //if (Input.GetKeyDown(KeyCode.B))
        //{
        //    Brakes(true);
        //}

        //if (Input.GetKeyUp(KeyCode.B))
        //{
        //    Brakes(false);

        //}
        //if (Input.GetKeyDown(KeyCode.M))
        //{
        //    BoostUp(3,Vector3.up);
        //}
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began )
            {
                if (tapCount == 1 && tapCd > 0.0f)
                {
                    //double tap
                    boost();
                }
                else
                {

                    //single tap
                    tapCd = 0.5f;
                    tapCount++;
                    Brakes(true);
                }
            }
            if (touch.phase == TouchPhase.Ended)
            {
                Brakes(false);
            }

        }
        //CameraDistanceOffset.transform.localPosition = new Vector3(0, 0, -15.0f - this._rigidBody.velocity.magnitude * 15.0f);

        if (tapCd > 0)
        {
            tapCd -= Time.deltaTime;
        }
        else
        {
            tapCount = 0;
        }

        if(_boostAddition > 0)
        {
            _boostAddition -= (Time.deltaTime * 5);
        }
    }

    private void FixedUpdate()
    {
        //LimitVelocity();

        
        //Vector3 v = _rigidBody.velocity;
        //float sqrBoostAddition = _boostAddition * _boostAddition;

        //if (v.sqrMagnitude > (sqrMaxVelocity + sqrBoostAddition))
        //{ 
        //    _rigidBody.velocity = v.normalized * (maximumVelocity + _boostAddition);
        //}
    }

    public void boost()
    {

        if(boostCD > 0.0f)
        {
            return;
        }
        boostCD = 2.0f;
        //return;
        Rigidbody rig = this.gameObject.GetComponent<Rigidbody>();
        //if (rig.useGravity == false)
        //{
        //    this.transform.SetParent(null);


        //    rig.useGravity = true;
        //    rig.velocity = transform.forward;
        //    rig.velocity *= -30.0f;
        //    StartCoroutine("unstuck");

        //    GameObject bb = Instantiate(boostObj, new Vector3(0, 0, 0), Quaternion.identity);
        //    bb.GetComponent<boostScrip>().parent = car;
        //    bb.SetActive(true);
        //    return;
        //}


        if (rig.velocity.magnitude < 0.25f)
        {
            rig.velocity *= 8.0f;
        }
        else if (rig.velocity.magnitude < 0.5f)
        {
            rig.velocity *= 5.0f;

        }
        else if (rig.velocity.magnitude < 1.2f)
        {
            rig.velocity *= 2.0f;

        }
        else
        {
            rig.velocity *= 1.5f;
        }
        
        GameObject nb = (GameObject)Instantiate(Resources.Load("Boost"), new Vector3(0, 0, 0), Quaternion.identity);
        nb.GetComponent<boostScrip>().transform.parent = car.transform;
        nb.SetActive(true);



        //Debug.Log(rig.velocity);
        //rig.velocity += rig.velocity.normalized;
    }

    public IEnumerator unstuck()
    {
        yield return new WaitForSeconds(0.25f);
        car.GetComponent<carScript>().isStuck = false;

    }

    private void LimitVelocity()
    {
        //if (this._rigidBody.velocity.magnitude > maximumVelocity)
        //{
        //    Vector3 normalizedValue = this._rigidBody.velocity.normalized;
        //    _rigidBody.velocity = normalizedValue * maximumVelocity;
        //}

        //if (this._rigidBody.velocity.magnitude > maximumVelocity)
        //{
        //    this._rigidBody.velocity = Vector3.ClampMagnitude(this._rigidBody.velocity, maximumVelocity);
        //}
    }

    public void BoostUp(float boostValue,Vector3 direction)
    {
        _boostAddition = boostValue;
        _rigidBody.velocity = direction;
        _rigidBody.velocity = _rigidBody.velocity.normalized * (maximumVelocity + _boostAddition);
    }

    private void Brakes(bool val)
    {
        if(val)
        {
            _rigidBody.drag = 2.0f;
            trailRenderers[0].emitting = true;
            trailRenderers[1].emitting = true;

        }
        else
        {
            _rigidBody.drag = 0.0f;
            trailRenderers[0].emitting = false;
            trailRenderers[1].emitting = false;
        }
    }
}

