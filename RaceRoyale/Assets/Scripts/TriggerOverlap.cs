﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerOverlap : MonoBehaviour
{

    [SerializeField] public LayerMask _layerMask = new LayerMask();
    [SerializeField] public UnityEvent _trigger;

    private void OnTriggerEnter(Collider other)
    {

        if (((1 << other.gameObject.layer) & _layerMask) != 0)
        {

            Debug.Log("ENTER TRIGGER");
            _trigger?.Invoke();
            TriggerOperation triggerOperation =  this.GetComponent<TriggerOperation>();
            if(triggerOperation)
            {
                triggerOperation.TriggerInvoker(other.gameObject);
            }
        }
    }
   
}
