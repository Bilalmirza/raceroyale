﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;

public class restart : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void restartScene()
    {
        if (SystemInfo.unsupportedIdentifier != SystemInfo.deviceUniqueIdentifier)
        {
            Analytics.CustomEvent(SystemInfo.deviceName);
            Analytics.CustomEvent(SystemInfo.deviceName, new Dictionary<string, object>
        {
            { "Restart", 1 }
        });
        }

        Analytics.CustomEvent("Restart");
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
