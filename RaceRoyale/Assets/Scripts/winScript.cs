﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class winScript : MonoBehaviour
{
    [SerializeField]
    GameObject blueFlag,redFlag,greenFlag;

    [SerializeField]
    timer _timer;
    [SerializeField]
    GameObject blueBall, redBall, greenBall;

    [SerializeField]
    public Text standings;
    [SerializeField]
    public Text highScore;
    bool isWin = false;
    public string playerColor;

    [SerializeField]
    ghostManager _ghostManager;
    [SerializeField] WebAccess wa;
    private int standingPos = 1;

    public const string PLAYER_HIGHSCORE_KEY = "PLAYER_HIGHSCORE_KEY";

    // Start is called before the first frame update
    void Start()
    {

        if (!PlayerPrefs.HasKey(PLAYER_HIGHSCORE_KEY))
        {
            PlayerPrefs.SetFloat(PLAYER_HIGHSCORE_KEY, 1000.0f);
            
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision collisionInfo)
    {
        //if(isWin)
        //{
        //    return;
        //}

       

        carScript carComponent = collisionInfo.collider.gameObject.GetComponentInChildren<carScript>();
        if(carComponent)
        {
            if (carComponent.hasWon == false)
            {
                if (collisionInfo.collider.gameObject.GetComponentInChildren<carScript>().CarColor == playerColor)
                {
                    if (_timer._currentTime < PlayerPrefs.GetFloat(PLAYER_HIGHSCORE_KEY))
                    {
                        PlayerPrefs.SetFloat(PLAYER_HIGHSCORE_KEY, _timer._currentTime);
                        _ghostManager.Save();

                        if(_timer._currentTime < wa.ghostScore)
                        {

                            wa.CreateText();
                        }
                    }
                    updateHighScore();
                    standings.text += "<size=90><b><color=yellow>";
                }
                standings.text += standingPos.ToString();
                standings.text += ". " + collisionInfo.collider.gameObject.GetComponentInChildren<carScript>().CarColor;
                standings.text += ": " + _timer._currentTime.ToString("0.00") + "\n";
                if (collisionInfo.collider.gameObject.GetComponentInChildren<carScript>().CarColor == playerColor)
                {
                    standings.text += "</color></b></size>";
                }
                carComponent.hasWon = true;
                Debug.Log("WINNER COLOR: " + collisionInfo.collider.gameObject.GetComponentInChildren<carScript>().CarColor);
                standingPos++;
               

                greenFlag.gameObject.SetActive(true);
                isWin = true;
            }
        }
        if (collisionInfo.collider.tag == "green")
        {
          
        }

        //if (collisionInfo.collider.tag == "red")
        //{
        //    redFlag.gameObject.SetActive(true);
        //    isWin = true;

        //}

        //if (collisionInfo.collider.tag == "blue")
        //{
        //    blueFlag.gameObject.SetActive(true);
        //    isWin = true;

        //}
        if(isWin)
        {
            _timer.end();
        }
    }

    void updateHighScore()
    {
        highScore.text = "HighScore: " + PlayerPrefs.GetFloat(PLAYER_HIGHSCORE_KEY).ToString("#.00");
    }
}
