﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;

public class timer : MonoBehaviour
{

    public float _currentTime;
    [SerializeField]
    public float _startingTime;
    [SerializeField]
    public Text _cdText;

    public bool hasEnded = false;
    // Start is called before the first frame update
    void Start()
    {
		//FirebaseManager.instance.leaderboardController.UpdateUserGameTime();
        _currentTime = _startingTime;
    }

    // Update is called once per frame
    void Update()
    {
        _currentTime += Time.deltaTime;

        if (!hasEnded)
        {
            _cdText.text = _currentTime.ToString("0.00");
            // Debug.Log(_currentTime);
        }
    }

    public void end()
    {
        if (!hasEnded)
        {
			//FirebaseManager.instance.leaderboardController.UpdateUserScore( ( (int)_currentTime ) );
			//FirebaseManager.instance.leaderboardController.UpdateUserGameTime( (int)_currentTime );
            Analytics.CustomEvent(SystemInfo.deviceName, new Dictionary<string, object>
        {
            { "Won", _currentTime }
        });
            //_cdText.transform.localScale = new Vector3(3.0f, 3.0f, 3.0f);
            hasEnded = true;
            Debug.Log("GAME ENDED");
        }
    }
}
