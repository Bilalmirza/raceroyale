﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GameController : MonoBehaviour
{
    // Start is called before the first frame update


    [SerializeField] public List<Transform> _tracks;
    static int _currentPos = 0;

    [SerializeField] public Text TrackName;

    [SerializeField] Camera camera;
    [SerializeField] timer _timer;


    void Start()
    {
        Application.targetFrameRate = 60;

        StartCoroutine("PauseNow");
        ActivateTrack();
    }

    public IEnumerator PauseNow()
    {
        yield return new WaitForSeconds(0.017f);
        Time.timeScale = 0;
        _timer._currentTime = 0.0f;

    }
    // Update is called once per frame
    void Update()
    {

    }

    void ActivateTrack()
    {
        for(int i = 0; i < _tracks.Count; i ++)
        {
            if( i == _currentPos)
            {
                _tracks[i].gameObject.SetActive(true);
                TrackName.text = _tracks[i].name;
            }
            else
            {
                _tracks[i].gameObject.SetActive(false);
            }
        }
    }
    public void Next()
    {
        _currentPos++;
        if(_currentPos > _tracks.Count - 1)
        {
            _currentPos = 0;
        }
        ActivateTrack();
    }

    public void Back()
    {
        _currentPos--;
        if (_currentPos < 0)
        {
            _currentPos = _tracks.Count - 1;
        }
        ActivateTrack();
    }

    public void Play()
    {
        Time.timeScale = 1.0f;
        camera.fieldOfView = 60.0f;

    }

}
