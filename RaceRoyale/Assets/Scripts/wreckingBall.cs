﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wreckingBall : MonoBehaviour
{
    // Start is called before the first frame update
     private Vector3 _rotationVector;
    [SerializeField] public Vector3 _startingRotation;

    private Vector3 _currentSpeed;
    void Start()
    {
        _rotationVector = new Vector3(1, 1, 1);
        this.transform.rotation = Quaternion.Euler(_startingRotation);
        _currentSpeed = new Vector3(0.1f, 0.1f, 0.1f);
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale == 0) return;

        _currentSpeed.z = this.transform.localRotation.z < 0 ? _currentSpeed.z + (_rotationVector.z * Time.deltaTime) : _currentSpeed.z - (_rotationVector.z * Time.deltaTime);
        Debug.Log(_currentSpeed);
        this.transform.Rotate(new Vector3(0,0, _currentSpeed.z + Time.deltaTime));

    }
}
